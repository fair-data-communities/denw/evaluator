module Sorbet
  module DefaultEvaluators
    module DenWDistribution
      include StandardNamespaces
      include DenWDefinitions

      attr_accessor :denw_name

      def protocol_info
        result = {
          application_protocol: nil,
          security_protocol: nil,
          file_format: nil
        }

        if endpoint_url.start_with?("https")
          result[:application_protocol] = :https
        elsif endpoint_url.start_with?("http")
          result[:application_protocol] = :http
        end

        wfs_predicate = RdfName.new(space: NS_DCAT, local: 'conformsTo')
        wfs = RdfName.new(space: "https://www.aquo.nl/index.php/Id-2f98e74d-698a-44d2-9be0-c03da68b860e")
        if match_names(wfs_predicate, wfs)
          result[:file_format] = :xml
        end

        authentication_predicate = RdfName.new(space: METADATA_DENW, local: 'Authentication')
        authentication = dataset.contains_predicate(authentication_predicate)
        if authentication&.name&.space == METADATA_DENW
          authentication_method = authentication.name.local
          if authentication_method == "Authentication-clientserverssl"
            result[:security_protocol] = :clientserverssl
          end
          if authentication_method == "Authentication-webpki"
            result[:security_protocol] = :webpki
          end
          if authentication_method == "Authentication-none"
            result[:security_protocol] = :none
          end
        end

        result
      end

      def endpoint_url
        return @endpoint_url if @endpoint_url

        endpoint_predicate = RdfName.new(space: NS_DCAT, local: 'accessURL')
        @endpoint_url = contains_predicate(endpoint_predicate)&.name&.to_s || ""
      end
    end
  end
end
