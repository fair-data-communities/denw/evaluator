require_relative "./metadata"
require_relative "./denw_definitions"
require_relative "./denw_dataset"
require_relative "./denw_distribution"
# require_relative "./validations"

module Sorbet
  module DefaultEvaluators
    # Interpretes metadata files. For now, this is more of an XML parser
    # than a RDF parser. However, this behavious may change in the future.
    class DenWMetadata < Metadata
      include DenWDefinitions
      include Validations

      def dataset_name
        RdfName.new(space: METADATA_DENW, local: "")
      end

      def extend_dataset(dataset)
        dataset.extend DenWDataset
      end

      def mappings_info(distribution_name)
        distribution = dataset.distribution(distribution_name)

        if distribution.nil?
          puts "Cannot find distribution file"
          return {
            missing: [],
            mandatory_found: 0,
            mandatory_total: 1
          }
        end

        distribution.mappings_info
      end

      def identification_info
        distribution_url_present = dataset.distributions.all? do |distribution|
          valid_value?(distribution&.endpoint_url)
        end

        {
          dataset_present: all_datasets.count.positive?,
          valid_dataset_present: datasets.count.positive?,
          distribution_present: dataset.distributions&.count&.positive? || false,
          distribution_contains_url: distribution_url_present
        }
      end

      def data_protocols
        result = {
          application_protocol: nil,
          security_protocol: nil,
          file_format: nil
        }
        protocols = {
          'locations': result,
          'parameters': result,
          'timeseries': result,
        }
        dataset&.distributions.map do |distribution|
          protocols[distribution.denw_name] = distribution.protocol_info
        end
        puts protocols
        protocols
      end
    end
  end
end
