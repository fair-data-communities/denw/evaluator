# require_relative "./rdf_name"
# require_relative "./namespaces"
require_relative "./denw_locations_distribution"
require_relative "./denw_parameters_distribution"
require_relative "./denw_timeseries_distribution"
# require_relative "./validations"

module Sorbet
  module DefaultEvaluators
    module DenWDataset
      include DenWDefinitions
      include StandardNamespaces
      include Validations

      attr_accessor :metadata
      attr_accessor :distributions

      def initialize
        super
        self.distributions = {
          locations: DenWLocationDistribution.new,
          parameters: DenWParametersDistribution.new,
          timeseries: DenWTimeseriesDistribution.new,
        }
      end

      # At least one dc:conformsto is present with the given name.
      def conforms_to(local, space = nil)
        space ||= METADATA_DENW
        conformsTo = RdfName.new(space: NS_DC, local: 'conformsTo')
        localName = RdfName.new(space: space, local: local)
        match_names(conformsTo, localName)
      end

      def legal_info
        {
          denw_2021: denw_license_present?
        }
      end

      def data_info
        result = {}

        available_until = contains_predicate(RdfName.new(space: METADATA_DENW, local: "AvailableUntil"))
        result[:available_until] = valid_value?(available_until&.name&.value)
        epsg7415_compliant = contains_predicate(RdfName.new(space: METADATA_DENW, local: "Epsg7415Compliant"))
        result[:epsg7415_compliant] = valid_value?(epsg7415_compliant&.name&.value)

        result
      end

      def server_info
        return @server_info if @server_info
        {
          capacity: server_capacity,
          uptime: server_uptime,
          restore: server_restore
        }
      end

      def security_info
        results = {
          mechanism: :unknown
        }

        results[:mechanism] = :client_server_ssl if match_names(denwTerm('Authentication'), denwTerm('Authentication-clientserverssl'))
        results[:mechanism] = :server_ssl if match_names(denwTerm('Authentication'), denwTerm('Authentication-none'))

        results
      end

      # Returns the distributions
      def distributions
        return @distributions if @distributions

        @distributions = []
        distribution_predicate = RdfName.new(space: NS_DCAT, local: 'distribution')
        distribution_list = contains_predicate_list(distribution_predicate)
        if distribution_list.empty?
          return @distributions
        end

        distribution_item_predicate = RdfName.new(space: NS_DCAT, local: 'Distribution')
        distribution_list.each do |distribution_listitem|
          distributionNodes = distribution_listitem.contains_predicate_list(distribution_item_predicate)
          distributionNodes.each do |distribution|
            distribution = metadata.resolve(distribution)
            distribution = extend_distribution(distribution)
            distribution.dataset = self
            @distributions << distribution
          end
        end

        @distributions
      end

      def extend_distribution(distribution)
        conformsTo = RdfName.new(space: NS_DCAT, local: 'conformsTo')

        if distribution.match_names(conformsTo, RdfName.new(space: METADATA_LOCATIONS_MAPPINGS))
          distribution.extend DenWLocationsDistribution
          distribution.denw_name = 'locations'
          return distribution
        end

        if distribution.match_names(conformsTo, RdfName.new(space: METADATA_PARAMETERS_MAPPINGS))
          distribution.extend DenWParametersDistribution
          distribution.denw_name = 'parameters'
          return distribution
        end

        if distribution.match_names(conformsTo, RdfName.new(space: METADATA_TIMESERIES_MAPPINGS))
          distribution.extend DenWTimeseriesDistribution
          distribution.denw_name = 'timeseries'
          return distribution
        end

        distribution.extend DenWDistribution
        distribution.extend Distribution
        return distribution
      end

      # Currently, only one distribution is supported
      def distribution(name = nil)
        distributions&.first if name.nil?

        distributions.each do |distribution|
          return distribution if distribution.denw_name == name
        end

        return nil
      end

      def provenance_info
        result = {
          functional: {},
          technical: {}
        }

        publisher_predicate = RdfName.new(space: NS_DC, local: 'publisher')
        publisher = contains_predicate(publisher_predicate)
        label_predicate = RdfName.new(space: NS_RDFS, local: 'label')
        label = publisher&.contains_predicate(label_predicate)
        contributor_predicate = RdfName.new(space: NS_DC, local: 'contributor')
        contributor = contains_predicate(contributor_predicate)
        result[:organisation] = valid_value?(label&.name&.value)
        result[:author] = valid_value?(contributor&.name&.value)

        functional_predicate = RdfName.new(space: METADATA_DENW, local: 'FunctionallyResponsible')
        foaf = contains_predicate(functional_predicate)
        if foaf
          foaf = metadata.resolve(foaf)
          result[:functional][:block] = true
          name = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'name'))
          result[:functional][:name] = valid_value?(name&.name&.value)
          email = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'mbox'))
          result[:functional][:email] = valid_value?(email&.name&.value)
        end

        technical_predicate = RdfName.new(space: METADATA_DENW, local: 'TechnicallyResponsible')
        foaf = contains_predicate(technical_predicate)
        if foaf
          foaf = metadata.resolve(foaf)
          result[:technical][:block] = true
          name = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'name'))
          result[:technical][:name] = valid_value?(name&.name&.value)
          email = foaf.contains_predicate(RdfName.new(space: NS_FOAF, local: 'mbox'))
          result[:technical][:email] = valid_value?(email&.name&.value)
        end

        result
      end

      private

      def server_capacity
        return :yes if match_names(denwTerm('ServerCapacity'), denwTerm('ServerCapacity-yes'))
        return :no if match_names(denwTerm('ServerCapacity'), denwTerm('ServerCapacity-no'))
        :unknown
      end

      def server_uptime
        return :yes if match_names(denwTerm('ServerUptime'), denwTerm('ServerUptime-yes'))
        return :no if match_names(denwTerm('ServerUptime'), denwTerm('ServerUptime-no'))
        :unknown
      end

      def server_restore
        return 24 if match_names(denwTerm('ServerRestore'), denwTerm('ServerRestore-24'))
        return :business24 if match_names(denwTerm('ServerRestore'), denwTerm('ServerRestore-business-hours'))
        return :long if match_names(denwTerm('ServerRestore'), denwTerm('ServerRestore-no'))
        :unknown
      end

      def denw_license_present?
        return @denw_license_present if @denw_license_present
        licensePredicate = RdfName.new(space: NS_DC, local: 'license')
        license = RdfName.new(space: "https://gitlab.com/fair-data-communities/denw/assets/-/raw/master/licenses/user_license.md")
        @denw_license_present = match_names(licensePredicate, license)
      end
    end
  end
end
