require_relative "./denw_metadata"

module Sorbet
  module DefaultEvaluators
    class DenwMetadataLoader
      attr_accessor :cache

      def configure(_)
        self.cache = {}
      end

      def load(url)
        return cache[url] if cache[url]

        result = DenWMetadata.new
        result.load_metadata(url)
        cache[url] ||= result
      end
    end
  end
end
