require_relative "./denw_definitions"
require_relative "./distribution"
require_relative "./denw_distribution"
#require_relative "./namespaces"

module Sorbet
  module DefaultEvaluators
    module DenWLocationsDistribution
      include DenWDefinitions
      include StandardNamespaces
      include Distribution
      include DenWDistribution

      def expected_mapping
        ["id", "locationX", "locationY", "localName"]
      end

      def mapping_space
        METADATA_LOCATIONS_MAPPINGS
      end
    end
  end
end
