# require_relative "./rdf_node"
# require_relative "./rdf_document"
# require_relative "./namespaces"

module Sorbet
  module DefaultEvaluators
    class Metadata < RdfNode
      include Sorbet::Evaluators::Service
      include RdfDocument
      include StandardNamespaces

      attr_accessor :file_status

      FILE_STATUS_OK      = "ok"
      FILE_STATUS_FAILED  = "failed"

      def initialize
        # Name is set by the configure method
        super(name: RdfName.new)
      end

      def configure(configuration)
        # load_metadata(configuration[:metadata_url])
      end

      def load_metadata(url)
        result = HttpSorbet.get(url)
        if (result[:code] / 100) != 2
          @file_status = FILE_STATUS_FAILED
          @nodes = []
          return
        end

        @file_status = FILE_STATUS_OK
        parsed_document = parse_rdf_document!(result[:body])
      end

      def all_datasets
        @all_datasets ||= types(RdfName.new(space: NS_DCAT, local: 'Dataset'))
      end

      # Returns all the RDF entities that match a certain type
      def types(type_name, list = nil)
        predicate = RdfName.new(space: NS_DC, local: 'type')
        list ||= nodes
        list.select do |node|
          node[:object].match_names(predicate, type_name)
        end
      end

      def protocol_info
        {
          application_layer: :https,
          file_format: :rdf_xml
        }
      end

      def resolve(node)
        if node.resource_node && node.name.space == ""
          nodes.each do |document_node|
            object = document_node[:object]
            if object.name.local == node.name.local
              return object
            end
          end
        end

        return node
      end

      # Returns a single RDF entitity that matches the given name. If the node
      # cannot be found, the function returns nil.
      def by_name(local)
        nil
      end

      # Returns all the valid datasets. A valid dataset is a dataset which
      # has the following properties:
      # - conforms to appropriate definition
      def datasets
        unless respond_to?(:dataset_name)
          all_datasets
        end

        types(dataset_name, all_datasets)
      end

      def mappings_info(distribution)
      end

      # Returns the first dataset of the datasets.
      def dataset
        return @dataset if @dataset

        @dataset = (datasets&.first&.dig(:object) || RdfNode.new(name: RdfName.new()))
        extend_dataset(@dataset) if respond_to?(:extend_dataset)
        @dataset.metadata = self
        @dataset
      end
    end
  end
end
