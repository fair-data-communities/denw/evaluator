module Sorbet
  module DefaultEvaluators
    module Distribution
      include StandardNamespaces

      attr_accessor :node
      attr_accessor :dataset

      # list of expected mappings to be present. Mappings are encoded
      # as their local names
      def expected_mapping
        []
      end

      # Namespace of the mappings
      def mapping_space
        ""
      end

      # Returns the mapping info
      def mappings_info
        expected = expected_mapping
        found = []

        owl_class_predicate = RdfName.new(space: NS_OWL, local: "Class")
        owl_sameas_predicate = RdfName.new(space: NS_OWL, local: "sameAs")
        ppb_constant_predicate = RdfName.new(space: NS_PPB, local: "constant")
        type_predicate = RdfName.new(space: NS_RDF, local: "type")
        dataset.metadata.nodes.each do |node|
          if node[:predicate].name.equals(owl_class_predicate)
            owl_class = node[:object]

            # mapping of owl:Class to field
            sameas_list = owl_class.contains_predicate_list(owl_sameas_predicate)
            sameas_list.each do |sameas|
              if sameas.name.space == mapping_space
                name = sameas.name.local
                found << name if expected.include?(name)
              end
            end

            # mapping of constant to field
            ppb_constants = owl_class.contains_predicate_list(ppb_constant_predicate)
            ppb_constants.each do |ppb_constant|
              type_value = ppb_constant.contains_predicate(type_predicate)
              if type_value && type_value.name.space == mapping_space
                name = type_value.name.local
                found << name if expected.include?(name)
                # TODO: klopt de resource waarde wel?
              end
            end
          end
        end

        result = {
          mandatory_found: found.length,
          mandatory_total: expected.length,
          missing: [
          ]
        }

        (expected - found).each do |name|
          result[:missing] << {
            name: name,
            mandatory: true
          }
        end

        result
      end
    end
  end
end
