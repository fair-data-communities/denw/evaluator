require_relative "./denw_definitions"
require_relative "./distribution"
require_relative "./denw_distribution"
#require_relative "./namespaces"

module Sorbet
  module DefaultEvaluators
    module DenWParametersDistribution
      include DenWDefinitions
      include StandardNamespaces
      include Distribution
      include DenWDistribution

      attr_accessor :dataset

      def expected_mapping
        ["id", "localName", "name", "unitOfMeasurement", "localUnitOfMeasurement", "localMeasurementType", "measurementType"]
      end

      def mapping_space
        METADATA_PARAMETERS_MAPPINGS
      end
    end
  end
end
