# rdf_name was already loaded
# require "sorbet/rdf_name"

module Sorbet
  module DefaultEvaluators
    module DenWDefinitions
      METADATA_DENW_URL             = 'https://gitlab.com/fair-data-communities/denw/assets/-/raw/master/definitions/'
      METADATA_DENW                 = METADATA_DENW_URL + 'denw.rdf'
      METADATA_DENW_DEFINITION      = METADATA_DENW_URL + 'denw.rdf'
      METADATA_LOCATIONS_MAPPINGS   = METADATA_DENW_URL + 'locations_md.rdf'
      METADATA_TIMESERIES_MAPPINGS  = METADATA_DENW_URL + 'timeseries_md.rdf'
      METADATA_PARAMETERS_MAPPINGS  = METADATA_DENW_URL + 'parameters_md.rdf'

      def denwTerm(local)
        RdfName.new(space: METADATA_DENW, local: local)
      end
    end
  end
end
