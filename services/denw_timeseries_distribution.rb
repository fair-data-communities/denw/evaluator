require_relative "./denw_definitions"
require_relative "./distribution"
require_relative "./denw_distribution"
#require_relative "./namespaces"

module Sorbet
  module DefaultEvaluators
    module DenWTimeseriesDistribution
      include DenWDefinitions
      include StandardNamespaces
      include Distribution
      include DenWDistribution

      attr_accessor :dataset

      def expected_mapping
        ["locationId", "parameterId", "events"]
      end

      def mapping_space
        METADATA_TIMESERIES_MAPPINGS
      end
    end
  end
end
