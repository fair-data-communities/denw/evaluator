require "spec_helper"

context "DenWEvaluator2021", type: :model do
  include EvaluatorSetup
  include DenWEvaluator

  describe "Reusable" do
    it "must give full points" do
      # Given
      setup_evaluator fixture: "all.xml"

      # When
      results = context.execute(variables)
      results.base_metrics = ["R"]

      # Then
      rulings = results.perform_rulings
      puts results.feedback
      expect(feedback_contains_errors?(results)).to eq false
      expect(rulings["R"]).to eq 1.0
    end

    it "R1.1: license was not accepted" do
      # Given
      setup_evaluator fixture: "simple_metadata.xml"

      # When
      results = context.run_only(:"R1.1", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :license_not_accepted
    end

    it "R1.2: provenance was not provided" do
      # Given
      setup_evaluator fixture: "simple_metadata.xml"

      # When
      results = context.run_only(:"R1.2", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
      expect(results.feedback.count).to eq 2
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :provenance_organisation_unknown
      expect(results.feedback[1][:type]).to eq :recommendation
      expect(results.feedback[1][:comment]).to eq :provenance_author_unknown
    end
  end
end
