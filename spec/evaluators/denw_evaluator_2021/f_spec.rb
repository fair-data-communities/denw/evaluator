require "spec_helper"

context "DenWEvaluator2021", type: :model do
  include EvaluatorSetup
  include DenWEvaluator

  let(:url_without_uuid)          { "https://purplepolarbear.nl/metadata/1234" }
  let(:fixture_no_valid_dataset)  { "no_valid_dataset.xml" }

  describe "Findable" do
    it "must give full points" do
      # Given
      setup_evaluator

      # When
      results = context.run_only(:F, variables)
      results.base_metrics = ["F"]

      # Then
      rulings = results.perform_rulings
      expect(feedback_contains_errors?(results)).to eq false
      puts results.feedback
      expect(rulings["F"]).to eq 1.0
    end

    it "F1: no UUID was provided" do
      # Given
      setup_evaluator(url: url_without_uuid)

      # When
      results = context.run_only(:F1, variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0.0
    end

    it "F3: no planning dataset was provided" do
      # Given
      setup_evaluator(fixture: fixture_no_valid_dataset)

      # When
      results = context.run_only(:F3, variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0
      expect(results.feedback.count).to eq 1
      expect(results.feedback[0][:type]).to eq :recommendation
      expect(results.feedback[0][:comment]).to eq :no_denw_type_specified
    end
  end
end
