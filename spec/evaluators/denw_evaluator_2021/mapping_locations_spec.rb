require "spec_helper"

context "DenWEvaluator2020", type: :model do
  include EvaluatorSetup
  include DenWEvaluator

  describe "DenW compliancy" do
    it "DenW: mappings for locations present" do
      # Given
      setup_evaluator fixture: "mapping_location.xml"

      # When
      results = context.run_only(:"DenW3.2", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 1.0
      expect(results.feedback.count).to eq 0
    end

    it "DenW: missing mappings for locations" do
      # Given
      setup_evaluator fixture: "mapping_empty.xml"

      # When
      results = context.run_only(:"DenW3.2", variables)

      # Then
      value = results.results.values[0]
      expect(feedback_contains_errors?(results)).to eq false
      expect(value).to eq 0
      expect(results.feedback).to contains_missing_mappings([
        "id",
        "locationX",
        "locationY",
        "localName"
      ])
    end
  end
end
