module EvaluatorSetup
  attr_accessor :evaluator_filename
  attr_accessor :variables

  # def setup_evaluator(evaluator_filename:, url:)
  #   self.evaluator_filename = evaluator_filename
  #   self.variables = { url: url }
  # end

  def evaluator
    return @evaluator if @evaluator

    root_path = File.join(__dir__, "../..")
    content = JSON.parse(File.read(File.join(root_path, "evaluators", evaluator_filename)), symbolize_names: true)
    resolvers = [
      { name: "me", path: root_path },
      { name: "default", path: "~/purple_polar_bear/fairbear/evaluator/store/evaluators/68ac217a-546a-4889-a7d6-ac82ca2c0316" },
    ]
    template_evaluator = Sorbet::Template::Evaluator.new(content.merge(resolvers: resolvers))
    builder = Sorbet::Template::ModelBuilder.new
    evaluator = builder.build(template_evaluator)
  end

  def context
    @context ||= Sorbet::Runner::Context.new(evaluator)
  end
end
