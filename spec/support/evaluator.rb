require "active_support/concern"

module DenWEvaluator
  extend ActiveSupport::Concern

  included do
    let(:url_default)               { "https://purplepolarbear.nl/metadata/" + SecureRandom.uuid }

    def setup_evaluator(fixture: "metadata.xml", url: nil, evaluator_filename: "denw_evaluator_2021.json")
      body = File.read(File.join(__dir__, "../fixtures/", fixture))
      uri = url || url_default
      Sorbet::DefaultEvaluators::HttpSorbet.instance.setup(url: uri, body: body)
      self.evaluator_filename = evaluator_filename
      self.variables = { url: uri  }
    end
  end
end
