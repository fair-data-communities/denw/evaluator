RSpec::Matchers.define :contains_missing_mappings do |expected|
  match do |actual|
    if expected.count != actual.count
      next false
    end

    result, _ = contain_missing_mappings_find_failure(expected, actual)
    result
  end

  failure_message do |actual|
    if expected.count != actual.count
      next "Incorrect number of feedback messages. Expected: #{expected}. Found: #{actual}"
    end

    result, actual_message = contain_missing_mappings_find_failure(expected, actual)
    "Expected mappings: #{expected}. Message found: #{actual_message}"
  end
end

def contain_missing_mappings_find_failure(expected, actual)
  actual.each do |actual_message|
    if actual_message[:type] != :recommendation
      return false, actual_message
    end

    if actual_message[:comment] != :missing_mapping
      return false, actual_message
    end

    if !expected.include?(actual_message[:vars][:name])
      return false, actual_message
    end

  end

  return true, nil
end
