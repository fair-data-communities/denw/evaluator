# Evaluator

## Metrics
De volgende metrics worden berekend:
- F
- A
- I
- R
- DenW
- Aquo

## Findable

Punten berekening

| Test | Gewicht | Blokkerend |
| -- | -- | -- |
| F1 | 1 | Ja |
| F2 | 1 | Ja |
| F3 | 1 | Ja |

### F1
Metadata krijgen een wereldwijd unieke en blijvende persistent identifier toegewezen. Dit wordt geimplementeerd door een URL die een UUID bevat. Deze UUID dient uniek te zijn voor ieder metadata bestand.

Tevens dienen alle URLs vanuit een beveiligde verbinding uit te gaan. Dit wordt geimplementeerd door https verplicht te stellen.

Scores

| Resultaat | punten |
| -- | -- |
| Een UUID kan gevonden worden in de URL. | 1 |
| De UUID kan niet gevonden worden. | 0 |

### F2
Gegevens worden beschreven met rijke metadata. Dit punt wordt gecheckt door de score van de DenW metric.

| Resultaat | punten |
| -- | -- |
| Score DenW metric voldoende | 1 |
| Score DenW metric onvoldoende | 0 |

### F3
Metadata gegevens bevatten duidelijk en expliciet de identificatie van de gegevens die ze beschrijven.

Hiervoor dient de metadata de volgende zaken te beschrijven:
- Er dient een dataset beschreven te zijn
- Deze dataset dient compliant te zijn met DenW parameters, locations en timeseries
- De dataset dient een distributie te bevatten
- De distributie dient een endpoint op te geven

Scores

| Resultaat | punten |
| -- | -- |
| De metadata geeft alle benodigde informatie | 1 |
| De metadata geeft niet alle benodigde informatie | 0 |

### F4
Metadata gegevens worden geregistreerd of geïndexeerd in een doorzoekbare bron. Dit is het distributiebestand.

Deze vraag wordt dus genegeerd, hier worden geen punten voor gegeven.


## Accessible

Punten berekening

| Test | Gewicht | Blokkerend |
| -- | -- | -- |
| A1 | 1 | Ja |
| A3 | 1 | Nee |

### A1
Metadata gegevens kunnen worden opgehaald door hun identificatie met behulp van een gestandaardiseerd communicatieprotocol. De ondersteunde protocollen voor de DenW community zijn:

Formaten:
- RDF/ XML

Applicatielaag protocol:
- HTTPS

Scores

| Resultaat | punten |
| -- | -- |
| Geaccepteerd applicatielaag protocol | 0.2 |
| Geen geaccepteerd applicatielaag protocol | 0 |
| Geaccepteerd data formaat | 0.8 |
| Geen geaccepteerd data formaat | 0 |


### A1.1
Het protocol is open, vrij en universeel implementeerbaar. Dit geldt voor alle protocollen zoals genoemd in A1.

Hier wordt dus geen score voor gegeven, aangezien dit al in A1 wordt afgevangen.

### A1.2
Het protocol maakt waar nodig een authenticatie- en autorisatieprocedure mogelijk. Dit is het geval met HTTP en HTTPS.

Hier wordt dus geen score voor gegeven, aangezien dit al in A1 wordt afgevangen.

### A2
Metadata zijn toegankelijk, ook als de data niet meer beschikbaar zijn. Dit is impliciet in de opzet.

Hier wordt dus geen score voor gegeven, aangezien dit al op andere punten wordt afgevangen.

### A3

Metadata bevat informatie over de beschikbaarheid en capaciteit van de data.

De score wordt berekend als het gemiddelde van de volgende:
- A3.1
- A3.2
- A3.3

### A3.1

De server heeft voldoende capaciteit om mee te doen aan de samenwerking.

De score wordt berekend aan de hand van de waarde van het DenW:ServerCapacity veld voor de dataset.

| Resultaat | punten |
| --- | --- |
| ServerCapacity-yes | 1 |
| ServerCapacity-no | 0 |
| niet gevonden/ niet ingevuld | 0 |

### A3.2

Is de server in principe altijd operationeel (>99% van de tijd)?

De score wordt berekend aan de hand van de waarde van het DenW:ServerUptime veld voor de dataset.

| Resultaat | punten |
| --- | --- |
| ServerUptime-yes | 1 |
| ServerUptime-no | 0 |
| niet gevonden/ niet ingevuld | 0 |

### A3.3

Wordt de server binnen 24 uur restored zodat de data altijd tijdige weer benaderbaar is voor de samenwerking?

| Resultaat | punten |
| -- | -- |
| ServerRestore-24 | 1 |
| ServerRestore-businnes-hours | 1 |
| ServerRestore-no | 0 |


## Interoperable

Punten berekening

| Test | Gewicht | Blokkerend |
| -- | -- | -- |
| I1 | 1 | Ja |
| I2 | 1 | Ja |

### I1
Metadata gebruiken een formele, toegankelijke, gedeelde en breed toepasbare taal voor kennisrepresentatie. Dit wordt ingevuld door de metadata templates. De metadata template dient als basis voor de kennisrepresentatie gebruikt te worden via een owl:theme.

Scores

| Resultaat | punten |
| -- | -- |
| Metadata template wordt gebruikt | 1 |
| Metadata template wordt niet gebruikt | 0 |

### I2
Metadata gebruikt vocabulaires die FAIR principes volgen. Dit volgt impliciet uit de metadata template.

In de metadatatemplate wordt verwezen naar de volgende standaarden:
- Aquo

Het vocabulair is gedefinieerd in de DenW metadata template en de DenW metric scoort deze. Dit is de waarde die gebruikt zal worden voor I2. Linken naar andere vocabulairs gebeurt impliciet via de DenW metadata template.

Scores

De score van DenW wordt gebruikt

### I3
Metadata bevat gekwalificeerde verwijzingen naar andere metadata. De metadata dient naar de metadata templates te verwijzen. Die verwijzen op haar beurt weer naar de volgende standaarden:
- Aquo

Hier wordt dus geen score voor gegeven, aangezien dit door I1 wordt afgevangen.

## Reusable

Punten berekening

| Test | Gewicht | Blokkerend |
| -- | -- | -- |
| R1 | 1 | Nee |
| R1.1 | 1 | Ja |
| R1.2 | 1 | Ja |
| R1.3 | 0 | Nee |

### R1
Metadata heeft meerdere nauwkeurige en relevante attributen. Een attribuut is relevant als zij door de DenW template ondersteund wordt. De score voor deze metric halen we uit de DenW score.

### R1.1
Metadata wordt vrijgegeven met een duidelijke en toegankelijke datagebruik licentie. De metadata dient de DenW licentie te gebruiken.

Scores

| Resultaat | punten |
| -- | -- |
| Er wordt gebruik gemaakt van de DenW License 2021 | 1 |
| Er wordt niet gebruik gemaakt van de DenW License 2021 | 0 |

### R1.2
Metadata zijn geassocieerd met hun herkomst. Dit vatten wij op dat het mogelijk is om de provenance te bepalen van de organisatie en van de medewerker die de laatste wijziging aan de ontologie beschrijving gemaakt heeft.

Scores

| Resultaat | punten |
| -- | -- |
| Provenance van de organisatie is aanwezig | 0.8 |
| Provenance van de organisatie is niet aanwezig | 0 |
| Provenance van de medewerker is aanwezig | 0.2 |
| Provenance van de medewerker is niet aanwezig | 0 |

### R1.3
Metadata voldoen aan domeinrelevante gemeenschapsnormen. Dit is direct de score uit de DenW metric.

Scores

De score voor de DenW metric wordt gebruikt.

## DenW

Punten berekening

| Test | Gewicht | Blokkerend |
| -- | -- | -- |
| DenW1 | 1 | Ja |
| DenW2 | 1 | Ja |
| DenW3 | 6 | Ja |
| DenW4 | 1 | Ja |
| DenW5 | 1 | Ja |
| DenW6 | 4 | Ja |

### DenW1

Data wordt beschreven in een geaccepteerd protocol.

Geaccepteerde applicatielaag protocollen:
- HTTPS

Geaccepteerde beveiligingsmethoden:
- Server side SSL met WebPKI
- Dubbele SSL met interne PKI

Geaccepteerde data formaten:
- XML

| Resultaat | punten |
| -- | -- |
| Applicatielaag protocol wordt ondersteund | 0.3 |
| Applicatielaag protocol wordt niet ondersteund | 0 |
| Beveiligings protocol wordt ondersteund | 0.3 |
| Beveiligings protocol wordt niet ondersteund | 0 |
| Data formaat wordt ondersteund | 0.4 |
| Data formaat wordt niet ondersteund | 0 |

### DenW2

Data gegevens zijn beveiligd indien dit volgens de organisatie nodig is. In het dataplan staat de beveiligingsmethode. Deze dient ook daadwerkelijk geimplementeerd te zijn.

Voor de WebPKI oplossing gebeurt dit door te valideren dat er sprake is van een HTTPS verbinding.

Voor de dubbele SSL oplossing gebeurt dit door.

Scores

| Resultaat | punten |
| -- | -- |
| Beveiliging is conform plan | 1 |
| Beveiliging is niet conform plan | 1 |

### DenW3

Data gebruiken een formele, toegankelijke, gedeelde en breed toepasbare taal voor kennisrepresentatie zoals bij DenW afgestemd. Dit gebeurt door het kopppelen van de data velden aan de metadatatemplate. Deze koppeling dient in het metadatabestand aanwezig te zijn.

De volgende velden dienen aanwezig te zijn (timeseries):
- locationId
- parameterId
- startDate
- endDate
- events

Een event dient de volgende velden te hebben (events):
- date
- waarde

De volgende velden dienen aanwezig te zijn (locations):
- id
- localName
- locationX
- locationY

De volgende velden dienen aanwezig te zijn (parameters):
- id
- localName
- name
- localUnitOfMeasurement
- unitOfMeasurement
- localMeasurementType
- measurementType

Let op! Startdatum en einddatum dient beschreven te zijn. Hiervoor dient één, niet meer niet minder, formaat gekozen te worden. De formaten die ondersteund worden zijn:
1. UTC
1. Nederlandse tijd met zomertijd
1. Nederlandse tijd zonder zomertijd

Er zijn twee manieren om dit te coderen. Hierbij dienst steeds één manier gekozen te worden. De manieren zijn:
1. Een veld data zowel datum als tijd bevat
1. Twee velden, waarbij het ene veld de datum bevat en het andere veld de tijd.

Een lijst met voorbeelden:

| Type | Voorbeeld | Link
| -- | -- | -- |
| datum tijd UTC | 2021-04-21T16:34:00Z | https://www.w3.org/tr.note_datetime
| datum UTC | 21-04-2021 |
| tijd UTC | 16:34 |

Scores

| Resultaat | punten |
| -- | -- |
| Veld zijn beschreven | 1 |
| Veld zijn niet beschreven | 0 |

### DenW4
Data wordt vrijgegeven onder de DenW License 2021.

Scores

| Resultaat | punten |
| -- | -- |
| Er wordt gebruik gemaakt van de DenW License 2021 | 1 |
| Er wordt niet gebruik gemaakt van de DenW License 2021 | 0 |

### DenW5
Metadata voldoet aan de domeinrelevante gemeenschapsnormen die DenW heeft vastgesteld.

Hiervoor dienen de volgende velden beschreven te worden:
- Functioneel verantwoordelijk
- Technisch verantwoordelijk
- Datum beschikbaarheid
- Coordinatensysteem

Scores

| Resultaat | punten |
| -- | -- |
| Veld zijn beschreven | 1 |
| Veld zijn niet beschreven | 0 |


## Aquo

Voor evaluaties gaan we ervan uit dat de metadatatemplate de maximale verwijzing naar Aquo bevat.

We gebruiken voor de velden controle de score van DenW3

| Resultaat | punten |
| -- | -- |
| Score van DenW3 | 1 |
